#include "../inc/mapa.hpp"
#include "../inc/navy.hpp"
#include "../inc/canoe.hpp"
#include "../inc/submarine.hpp"
#include "../inc/carrier.hpp"
#include "../inc/player.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(){
  //Mapa inicio;
  //inicio.menu();
  Mapa mapa;
  mapa.read_txt();

  return 0;
}
