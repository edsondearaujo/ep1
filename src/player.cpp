#include "../inc/player.hpp"
#include "../inc/navy.hpp"
#include "../inc/canoe.hpp"
#include "../inc/submarine.hpp"
#include "../inc/carrier.hpp"
#include <iostream>
#include <string>

using namespace std;

Players::Players(){
}
Players::~Players(){
}
string Players::get_player_name(){
  return player_name;
}
void Players::set_player_name(string player_name){
  this->player_name = player_name;
}
int Players::get_life(){
  return life;
}
void Players::set_life(int life){
  this->life = life;
}
