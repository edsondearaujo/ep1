#include "../inc/navy.hpp"
#include <iostream>
#include <string>

using namespace std;

int Navy::get_x(){
  return x;
}
int Navy::get_y(){
  return y;
}
string Navy::get_ship(){
  return ship;
}

void Navy::set_x(int x){
  this->x = x;
}
void Navy::set_y(int y){
  this->y = y;
}
void Navy::set_ship(string ship){
  this->ship = ship;
}
