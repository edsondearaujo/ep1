#include "../inc/mapa.hpp"
#include "../inc/navy.hpp"
#include "../inc/canoe.hpp"
#include "../inc/submarine.hpp"
#include "../inc/carrier.hpp"
#include "../inc/player.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#define TAM 13

using namespace std;

Players player1;           //jogador1
Players player2;          //jogador 2
Canoe canoe_player1[6];              //Quantidade
Canoe canoe_player2[6];                // de
Submarine submarine_player1[4];        //barco
Submarine submarine_player2[4];         //de
Carrier carrier_player1[2];            //cada
Carrier carrier_player2[2];           //jogador
string Tabuleiro[TAM][TAM];

void Mapa::menu(){
  cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~~~ ~~ ~~~ ~~ ~~ ~ ~ ~ ~~~~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~ ~~~~ ~  ~ WELCOME ~  ~ ~~ ~~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~ ~ ~~ ~ ~ ~~~ ~~ ~~ ~~~ ~ ~~ ~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~ ~ ~ ~~ ~ ~~ ~ ~ ~ ~ ~~ ~ ~ ~ ~ ~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~ ~ ~  ~ BATTLESHIP OO/FGA ~  ~ ~ ~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~~ ~~~ ~ ~ 1/2019 ~ ~ ~~~ ~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~~~~ ~ ~ ~~ ~~ ~ ~ ~ ~ ~~~~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~ ~ ~ ~ ~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
  cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
  cout << endl;
  cout << endl;
  cout << endl;

}
//FUNÇÃO DE LEITURA E CONFIGURAÇÃO DO MAPA
  void Mapa::read_txt(){


    string line;
    string arq;
    string vazio;
    //string auxdata;
    string type_ship;

    ifstream arquivo_map;
    vector<string> auxdata;
    vector<string> data;

    arquivo_map.open("doc/map_1.txt");

    if(arquivo_map.is_open()){
      while(getline(arquivo_map, line)){
            if(line[0] != '#' || line.size() == 0){
              int i, j;
              char c = 'C';
              char s = 'S';
              char p = 'P';

              for(i=1; i<=TAM; i++){
                for(j=1; j<=TAM; j++){
                  Tabuleiro[i][j] =  " ";
                }
                cout << endl;
              }

            if(line == "1 11 canoa nada"){
              Tabuleiro[1][11] = c;
              Tabuleiro[6][4] = c;
              Tabuleiro[10][8] = c;
              Tabuleiro[10][10] = c;
              Tabuleiro[12][15] = c;
              Tabuleiro[12][12] = c;

              for(i=1; i<=TAM; i++){
                for(j=1; j<=TAM; j++){
                  cout << Tabuleiro[i][j] << c << "\b" << "|";
                }
                cout << endl;
              }
              cout << endl;

              cout << "Canoa Player 1 posicionada" << endl;
            }
          }
        }
    }else
        cout << "Não foi possível ler o arquivo" << endl;

      line.clear();
  }
