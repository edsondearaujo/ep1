#include "../inc/carrier.hpp"
#include "../inc/navy.hpp"

#include <iostream>
#include <string>

using namespace std;

Carrier::Carrier(){
}
Carrier::~Carrier(){
}
string Carrier::get_orientation(){
  return orientation;
}
void Carrier::set_orientation(string orientation){
  this->orientation = orientation;
}
