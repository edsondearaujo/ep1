#ifndef CARRIER_HPP
#define CARRIER_HPP

#include "../inc/navy.hpp"
#include <string>
#include <iostream>

using namespace std;

class Carrier: public Navy{
private:
  string orientation;

public:
  Carrier();

  string get_orientation();
  void set_orientation(string orientation);
  int life[4];

  ~Carrier();
};
#endif
