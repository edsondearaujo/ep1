#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <string>

using namespace std;

class Players{
private:
  string player_name;
  int life;
public:
  Players();
  string get_player_name();
  void set_player_name(string player_name);
  int get_life();
  void set_life(int life);
  ~Players();
};
#endif
