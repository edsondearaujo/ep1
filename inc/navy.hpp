#ifndef NAVY_HPP
#define NAVY_HPP

#include <string>

using namespace std;

class Navy{
private:
  int x;
  int y;
  string ship;

public:
  int get_x();
  void set_x(int x);

  int get_y();
  void set_y(int y);

  string get_ship();
  void set_ship(string ship);

};
#endif
