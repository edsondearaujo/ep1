#ifndef SUBMARINRE_HPP
#define SUBMARINRE_HPP

#include "../inc/navy.hpp"
#include <string>
#include <iostream>

using namespace std;

class Submarine: public Navy{
private:
  string orientation;

public:
  string get_orientation();
  void set_orientation(string orientation);
  int life[2];

  Submarine();
  ~Submarine();
};
#endif
